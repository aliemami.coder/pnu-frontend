<?php
session_start();
if($_SESSION['username']){
    session_destroy();
    session_unset();
    header("Location: ".$url."index.php");
}else{
    header("Location: ".$url."login.php");
}

?>