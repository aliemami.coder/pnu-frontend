<?php
include "header.php";

if(isset($_SESSION['username'])){
    header("Location: index.php");
}


if($_SERVER['REQUEST_METHOD'] == "POST"){

    if(isset($_POST['username']) && isset($_POST['password'])){

        $username = htmlspecialchars($_POST['username']);
        $password = md5(htmlspecialchars($_POST['password']));

        $query = "SELECT username FROM users WHERE username=:username AND password=:password";
        $stmt = $connect->prepare($query);
        $stmt->bindParam(':username',$username);
        $stmt->bindParam(':password',$password);
        $stmt->execute();        
        $userDetails = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userDetails != null){            
            $_SESSION['username'] = $userDetails['username'];
            $_SESSION['is_admin'] = $userDetails['is_admin'];
            if($userDetails['is_admin'] === 1){
                header("Location: dashboard/index.php");
            }else{
                header("Location:$url/technolife");
            }
        }else{
            $errors = 'اطلاعات وارد شده صحیح نمیباشد.';
        }

        

    }

}

?>
<div class="text-center mt-5 login">    
    <form action="login.php" method="POST" class="text-start mx-auto col-6 p-5 rounded border border-primary shadow-lg">

        <?php if(isset($_GET['status']) && $_GET['status'] == 'success'){ ?>
            <div class="alert alert-success" role="alert">
                <span>ثبت نام شما با موفقیت انجام شد لطفا وارد شوید</span>
            </div>
        <?php } ?>

        <?php if(isset($errors)){ ?>
            <div class="alert alert-danger" role="alert">
                <span><?php echo $errors; ?></span>
            </div>
        <?php } ?>

        <div class="mb-3">
            <label for="username" class="form-label">نام کاربری</label>
            <input type="text" class="form-control" id="username" required name="username">
        </div>       
        <div class="mb-3">
            <label for="password" class="form-label">رمزعبور</label>
            <input type="password" class="form-control" id="password" required name="password">
        </div>

        <button type="submit" class="btn btn-primary">ورود</button>
    </form>
</div>
<?php
include "footer.php";
?>
