<?php
include "header.php";

$stmt = $connect->prepare('SELECT * FROM sliders');
$stmt->execute();
$sliders = $stmt->fetchAll(PDO::FETCH_ASSOC);

$stmt2 = $connect->prepare('SELECT * FROM posts');
$stmt2->execute();
$posts = $stmt2->fetchAll(PDO::FETCH_ASSOC);


?>


<div id="primarySlider" class="carousel slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        $isFirst = true;
        $cnt = 0;
        foreach ($sliders as $slider) { ?>
            <li data-bs-target="#primarySlider" data-bs-slide-to="<?php echo $cnt++; ?>" class="<?php echo $isFirst ? 'active' : ''; ?>"></li>
        <?php
            $isFirst = false;
        } ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php
        $isFirst2 = true;
        foreach ($sliders as $key => $slider) {
        ?>
            <div class="carousel-item <?php echo $isFirst2 ? 'active' : ''; ?>">
                <img class="w-100 img-fluid" src="<?php echo "$url/technolife/uploads/sliders/" . $slider['image']; ?>" alt="First slide">
            </div>
        <?php
            $isFirst2 = false;
        } ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#primarySlider" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#primarySlider" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>


<!-- <div class="container-fluid my-5">
    <div class="row">
        <?php foreach ($posts as $post) { ?>
            <div class="col-12 col-md-6 col-lg-3 mt-5 boxCol">
                <div class="boxes bg-warning p-5 rounded-3 border border-dark">
                    <img class="img-fluid w-100 mb-4" height="200px" src="<?php echo "$url/technolife/uploads/posts/img/" . $post['img']; ?>" alt="<?php echo $post['title'] ?>">
                    <h3 class="text-center"><?php echo $post['title'] ?></h3>
                    <hr>
                    <p class="text-justify"><?php echo $post['content'] ?></p>
                    <a href="post.php?id=<?php echo $post['id']; ?>" class="btn btn-primary">
                    مشاهده نوشته
                    </a>
                </div>
            </div>
        <?php } ?>        
    </div>    


</div> -->


<div class="container my-5">

    <div class="row">
        <div class="col-6">
            <div class="boxes rounded border border-primary p-5 shadow bg-info">
                <h2>تستی</h2>
            </div>
        </div>

        <div class="col-6">
            <div class="boxes rounded border border-primary p-5 shadow bg-info">
                <h2>تستی</h2>
            </div>
        </div>

        <div class="col-6">
            <div class="boxes rounded border border-primary p-5 shadow bg-info">
                <h2>تستی</h2>
            </div>
        </div>
    </div>      

</div>



</div>
<?php
include "footer.php";
?>