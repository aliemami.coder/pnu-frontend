<?php
include "header.php";

if(isset($_GET['id'])){
    $id = htmlspecialchars($_GET['id']);
    $stmt= $connect->prepare('SELECT * FROM posts WHERE id=:id');
    $stmt->bindParam(':id',$id);
    $stmt->execute();
    $post = $stmt->fetch(PDO::FETCH_ASSOC);
}else{
    header("Location:$url/technolife");
}

?>

<div class="container my-5">

<img class="img-fluid w-100 mb-4" height="400px" src="<?php  echo "$url/technolife/uploads/posts/img/" . $post['img'];?>" alt="<?php echo $post['title'] ?>">

<h1><?php echo $post['title']; ?></h1>
<span>تاریخ انتشار: </span>
<span dir="ltr"><?php echo $post['created_at'];?></span>
<hr>
<p>
    <?php echo $post['content']; ?>
</p>


</div>


<?php
include "footer.php";
?>