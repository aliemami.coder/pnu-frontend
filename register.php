<?php
include "header.php";
if(isset($_SESSION['username'])){
    header("Location: index.php");
}
$errors = array();
if($_SERVER['REQUEST_METHOD'] == "POST"){

    if(isset($_POST['fullName']) && isset($_POST['username']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['password'])){

        $fullName = htmlspecialchars($_POST['fullName']);
        $username = htmlspecialchars($_POST['username']);
        $email = htmlspecialchars($_POST['email']);
        $phone = htmlspecialchars($_POST['phone']);
        $password = md5(htmlspecialchars($_POST['password']));
 /*************************************************************/
        $query = "SELECT * FROM users WHERE username=:username";
        $stmt = $connect->prepare($query);
        $stmt->bindParam(':username',$_POST['username']);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            array_push($errors, "نام کاربری وارد شده قبلا انتخاب شده است");
        }
 /*************************************************************/
        $query = "SELECT * FROM users WHERE email=:email";
        $stmt = $connect->prepare($query);
        $stmt->bindParam(':email',$_POST['email']);
        $stmt->execute();

        if($stmt->rowCount() > 0){
           array_push($errors, "ایمیل وارد شده قبلا انتخاب شده است");
        }
 /*************************************************************/
        $query = "SELECT * FROM users WHERE phone=:phone";
        $stmt = $connect->prepare($query);
        $stmt->bindParam(':phone',$_POST['phone']);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            array_push($errors, "شماره موبایل وارد شده قبلا انتخاب شده است");
        }
 /*************************************************************/
        if(count($errors) == 0){

            $sql = "INSERT INTO users (full_name, username, email, phone, password) VALUES (:full_name, :username, :email, :phone, :password)";

            $res = $connect->prepare($sql);
            $res->bindParam(":full_name",$fullName,PDO::PARAM_STR);
            $res->bindParam(":username",$username,PDO::PARAM_STR);
            $res->bindParam(":email",$email,PDO::PARAM_STR);
            $res->bindParam(":phone",$phone,PDO::PARAM_STR);
            $res->bindParam(":password",$password,PDO::PARAM_STR);
            $res->execute();
            header("Location: login.php?status=success");
            // $_GET['status']
        }
    }
 
}
// session_unset();
// session_destroy();

?>
<?php if(!isset($_SESSION['username'])) { ?>
<div class="text-center mt-5">    
    <form action="register.php" method="POST" class="text-start mx-auto col-6 p-5 rounded border border-primary shadow-lg">

        <?php 
        if(count($errors) > 0){
            foreach($errors as $error){
            ?>
            <div class="alert alert-danger mb-3" role="alert">
                <span><?php echo $error; ?></span>
            </div>
        <?php
            } 
        } ?>


        <div class="mb-3">
            <label for="fullName" class="form-label">نام و نام خانوادگی</label>
            <input type="text" class="form-control" id="fullName" required name="fullName">    
        </div>
        <div class="mb-3">
            <label for="username" class="form-label">نام کاربری</label>
            <input type="text" class="form-control" id="username" required name="username">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">ایمیل</label>
            <input type="email" class="form-control" id="email" required name="email">
        </div>
        <div class="mb-3">
            <label for="phone" class="form-label">شماره موبایل</label>
            <input type="text" class="form-control" id="phone" required name="phone">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">رمزعبور</label>
            <input type="password" class="form-control" id="password" required name="password">
        </div>

        <button type="submit" class="btn btn-primary">ثبت نام</button>
    </form>
</div>
<?php }else{ ?>

    <div class="text-center">

        <div class="alert alert-danger" role="alert">
            <h2>شما قبلا وارد شدید</h2>
        </div>

    </div>

<?php } ?>

<?php
include "footer.php";
?>
