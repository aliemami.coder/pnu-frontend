<?php
    include_once "sidebar.php";
?>
<?php
$errors = array();
if($_SERVER['REQUEST_METHOD'] == "POST"){

    if(isset($_POST['title']) && isset($_POST['content'])){

        if(strlen($_POST['title']) > 3 && strlen($_POST['content']) > 3){

            $title = htmlspecialchars($_POST['title']);
            $content = htmlspecialchars($_POST['content']);

            // create image path
            $target_dir = "../uploads/posts/img/";
            $fileName =  mt_rand(10000,99999) . $_FILES["primaryImg"]["name"];
            $target_file = $target_dir . $fileName;
            $uploadOk = 1;
            // get image file type
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // get file size for image validation
            $check = getimagesize($_FILES["primaryImg"]["tmp_name"]);
            if($check === true) {   
              $uploadOk = 0;           
              array_push($errors, "فایل آپلود شده مجاز نیست");
            }

            // Limit file size
            if ($_FILES["primaryImg"]["size"] > 5000000) {
                array_push($errors, "تصویر شما باید کمتر 5 مگابایت باشد");
                $uploadOk = 0;
            }

            // Limit File type
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                array_push($errors, "تصویر شما باید با یکی از فرمت های jpg, jpeg, png باشد.");
                $uploadOk = 0;
            }

            
            if($uploadOk === 1){

                move_uploaded_file($_FILES["primaryImg"]["tmp_name"], $target_file);

                $q = "SELECT id FROM users WHERE username=:username";
                $stmt2 = $connect->prepare($q);
                $stmt2->bindParam(':username',$_SESSION['username']);
                $stmt2->execute();
                $owner_id = $stmt2->fetch(PDO::FETCH_ASSOC)['id'];

                $query = "INSERT INTO posts (title,content,img,owner_id,created_at,updated_at) VALUES (:title,:content,:img,:owner_id,:created_at,:updated_at)";
                $stmt = $connect->prepare($query);
                $stmt->bindParam(':title',$title);
                $stmt->bindParam(':content',$content);
                $stmt->bindParam(':img',$fileName);
                $stmt->bindParam(':owner_id',$owner_id);
                $stmt->bindParam(':created_at',date('Y-m-d H:i:s'));
                $stmt->bindParam(':updated_at',date('Y-m-d H:i:s'));
                $stmt->execute();            
                $success = "نوشته با موفقیت ایجاد شد.";  
            }          

        }else{            
            array_push($errors,'طول عنوان و توضیحات نوشته باید بیشتر از 3 کاراکتر باشد.');            
        }


    }else{
        array_push($errors, 'وارد کردن عنوان و توضیحات برای نوشته الزامی است.');
    }

}



?>
<div class="col-10 dashboard-createPost pt-5">
    <div class="container mt-5">   
        <?php 
        if(isset($errors) && count($errors) > 0){
            foreach($errors as $error){ ?>

            <div class="alert alert-danger" role="alert">
                <span><?php echo $error; ?></span>
            </div>
                                    
        <?php
            }
        } ?>

        <?php if(isset($success)){ ?>
            <div class="alert alert-success" role="alert">
                <span><?php echo $success; ?></span>
            </div>
            
        <?php } ?>

        <form action="createPost.php" method="POST" enctype="multipart/form-data">
            <div class="mb-3">
              <label class="custom-file">
                <input type="file" name="primaryImg" id="primaryImg" class="custom-file-input" accept="image/.jpg,.png,.jpeg">
                <span class="custom-file-control"></span>
              </label>
            </div>
            <div>
                <label for="title">عنوان نوشته</label>
                <input name="title" id="title" type="text" class="form-control mt-2">
            </div>
            <div class="mt-4">
                <label for="content">توضیحات نوشته</label>
                <textarea class="form-control mt-2" name="content" id="content" cols="30" rows="10"></textarea>
            </div>
            <button type="submit" class="btn btn-success mt-5">
                <span>ذخیره تغییرات</span>
                <i class="fas fa-save fa-lg fa-fw align-middle"></i>
            </button>
        </form>
    </div>
</div>

<?php
    include_once "sidebar2.php"
?>