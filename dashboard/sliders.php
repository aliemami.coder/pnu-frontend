<?php
include_once "sidebar.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $errors = array();
    if (isset($_POST['title']) && isset($_FILES['sliderImage'])) {

        $title = htmlspecialchars($_POST['title']);


        // create image path
        $target_dir = "../uploads/sliders/";
        $fileName =  mt_rand(10000, 99999) . $_FILES["sliderImage"]["name"];
        $target_file = $target_dir . $fileName;
        $uploadOk = 1;
        // get image file type
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        // get file size for image validation
        $check = getimagesize($_FILES["sliderImage"]["tmp_name"]);
        if ($check === true) {
            $uploadOk = 0;
            array_push($errors, "فایل آپلود شده مجاز نیست");
        }

        // Limit file size
        if ($_FILES["sliderImage"]["size"] > 5000000) {
            array_push($errors, "تصویر شما باید کمتر 5 مگابایت باشد");
            $uploadOk = 0;
        }

        // Limit File type
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            array_push($errors, "تصویر شما باید با یکی از فرمت های jpg, jpeg, png باشد.");
            $uploadOk = 0;
        }

        if ($uploadOk === 1) {

            move_uploaded_file($_FILES["sliderImage"]["tmp_name"], $target_file);

            $stmt = $connect->prepare('INSERT INTO sliders (title,image) VALUES (:title,:image)');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':image', $fileName);
            $stmt->execute();

            header("Location:$url/technolife/dashboard/sliders.php?status=1");
        }
    }
}

$stmt2 = $connect->prepare('SELECT * FROM sliders');
$stmt2->execute();
$sliders = $stmt2->fetchAll(PDO::FETCH_ASSOC);




?>

<div class="col-10 mt-5" id="sliders">
    <div class="container mt-5">
        <h3>مدیریت اسلایدرها</h3>
        <hr>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-lg mb-5" data-bs-toggle="modal" data-bs-target="#sliderModal">
            ایجاد اسلایدر جدید
        </button>

        <!-- Modal -->
        <div class="modal fade" id="sliderModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form action="sliders.php" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="mb-5">
                                <label for="title" class="form-label">عنوان اسلایدر</label>
                                <input type="text" class="form-control" name="title" id="title" required>
                            </div>

                            <div class="my-3">
                                <label class="custom-file">
                                    <input type="file" name="sliderImage" id="sliderImage" placeholder="تصویر خود را انتخاب کنید" class="custom-file-input" required>
                                    <span class="custom-file-control"></span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">لغو</button>
                            <button type="submit" class="btn btn-primary">ایجاد اسلایدر</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php if (isset($_GET['status']) && $_GET['status'] == '1') { ?>
            <div class="alert alert-success" role="alert">
                <span>اسلایدر شما با موفقیت ایجاد شد</span>
            </div>
        <?php } ?>

        <?php if (isset($errors) && count($errors) > 0) {
            foreach ($errors as $error) {
        ?>
                <div class="alert alert-success" role="alert">
                    <span><?php echo $error; ?></span>
                </div>
        <?php }
        } ?>

        <div class="table-responsive mt-5">

            <table class="table table-dark table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center align-middle">#</th>
                        <th class="text-center align-middle">تصویر</th>
                        <th class="text-center align-middle">عنوان</th>
                        <th class="text-center align-middle">عملیات</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sliders as $key => $slider) { ?>
                        <tr>
                            <td class="text-center align-middle" scope="row"><?php echo ++$key ?></td>
                            <td class="text-center align-middle">
                                <img src="<?php echo "$url/technolife/uploads/sliders/" . $slider['image']; ?>" class="img-thumbnail" width="150px" height="150px" alt="<?php echo $slider['title']; ?>">
                            </td>
                            <td class="text-center align-middle"><?php echo $slider['title']; ?></td>
                            <td class="text-center align-middle"></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>



        </div>


    </div>
</div>


<?php
include_once "sidebar2.php"
?>