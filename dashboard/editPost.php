<?php
include_once "sidebar.php";
?>
<?php

if($_SERVER['REQUEST_METHOD'] == "GET"){
    if (isset($_GET['id'])) {

        $postId = htmlspecialchars($_GET['id']);
    
        $stmt = $connect->prepare('SELECT * FROM posts WHERE id=:id');
        $stmt->bindParam(':id', $postId, PDO::PARAM_INT);
        $stmt->execute();
        $post = $stmt->fetch(PDO::FETCH_ASSOC);

    }else{
        header("Location: $url/pnu-frontend/dashboard/managePosts.php");
    }
}


$errors = array();
if ($_SERVER['REQUEST_METHOD'] == "POST") {  

    if (isset($_POST['title']) && isset($_POST['content']) && isset($_POST['id'])) {
        if (strlen($_POST['title']) > 3 && strlen($_POST['content']) > 3) {
            
            $postId = htmlspecialchars($_POST['id']);
            $title = htmlspecialchars($_POST['title']);
            $content = htmlspecialchars($_POST['content']);

            if(isset($_POST['primaryImg'])){
                // create image path
                $target_dir = "../uploads/posts/img/";
                $fileName =  mt_rand(10000, 99999) . $_FILES["primaryImg"]["name"];
                $target_file = $target_dir . $fileName;
                $uploadOk = 1;
                // get image file type
                $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

                // get file size for image validation
                $check = getimagesize($_FILES["primaryImg"]["tmp_name"]);
                if ($check === true) {
                    $uploadOk = 0;
                    array_push($errors, "فایل آپلود شده مجاز نیست");
                }

                // Limit file size
                if ($_FILES["primaryImg"]["size"] > 5000000) {
                    array_push($errors, "تصویر شما باید کمتر 5 مگابایت باشد");
                    $uploadOk = 0;
                }

                // Limit File type
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                    array_push($errors, "تصویر شما باید با یکی از فرمت های jpg, jpeg, png باشد.");
                    $uploadOk = 0;
                }
            }else{
                $fileName = $_POST['old_img'];
                $uploadOk = 1;
            }

            if ($uploadOk === 1) {
                if(isset($_POST['primaryImg'])){
                    move_uploaded_file($_FILES["primaryImg"]["tmp_name"], $target_file);
                }

                $query = "UPDATE posts SET title=:title,content=:content,img=:img,updated_at=:updated_at WHERE id=:id";
                $stmt = $connect->prepare($query);
                $stmt->bindParam(':id', $postId);
                $stmt->bindParam(':title', $title);
                $stmt->bindParam(':content', $content);
                $stmt->bindParam(':img', $fileName);                
                $stmt->bindParam(':updated_at', date('Y-m-d H:i:s'));
                $stmt->execute();
                header("Location:$url/pnu-frontend/dashboard/managePosts.php?success=نوشته با موفقیت ویرایش شد.");                
            }
        } else {
            array_push($errors, 'طول عنوان و توضیحات نوشته باید بیشتر از 3 کاراکتر باشد.');
        }
    } else {
        array_push($errors, 'وارد کردن عنوان و توضیحات برای نوشته الزامی است.');
    }
}



?>
<div class="col-10 dashboard-createPost pt-5">
    <div class="container mt-5">
        <?php
        if (isset($errors) && count($errors) > 0) {
            foreach ($errors as $error) { ?>

                <div class="alert alert-danger" role="alert">
                    <span><?php echo $error; ?></span>
                </div>

        <?php
            }
        } ?>

        <?php if (isset($success)) { ?>
            <div class="alert alert-success" role="alert">
                <span><?php echo $success; ?></span>
            </div>

        <?php } ?>

        <form action="editPost.php" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="d-none" value="<?php echo $_GET['id']; ?>" name="id">
            <div>
                <img class="shadow-sm rounded" width="350" height="250" src="<?php echo $url;?>/pnu-frontend/uploads/posts/img/<?php echo $post['img']; ?>" alt="<?php echo $post['title'] ?>">
            </div>
            <div class="my-3">
                <label class="custom-file">
                    <input type="hidden" class="d-none" value="<?php echo $post['img']; ?>" name="old_img">
                    <input type="file" name="primaryImg" id="primaryImg" class="custom-file-input" accept="image/*">
                    <span class="custom-file-control"></span>
                </label>
            </div>
            <div>
                <label for="title">عنوان نوشته</label>
                <input name="title" id="title" type="text" class="form-control mt-2" value="<?php echo $post['title']; ?>" required>
            </div>
            <div class="mt-4">
                <label for="content">توضیحات نوشته</label>
                <textarea required class="form-control mt-2" name="content" id="content" cols="30" rows="10"><?php echo $post['content']; ?></textarea>
            </div>
            <button type="submit" class="btn btn-success mt-5">
                <span>ذخیره تغییرات</span>
                <i class="fas fa-save fa-lg fa-fw align-middle"></i>
            </button>
        </form>
    </div>
</div>

<?php
include_once "sidebar2.php"
?>