<?php 
session_start();

if(!isset($_SESSION['username'])){
    header('Location: ../login.php');
}
if($_SESSION['is_admin']){
    header("Location: $url/technolife");
}

include_once "../database.php";
include_once "../generanDef.php";
?>
<!DOCTYPE html>
<html lang="fa_IR" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Techo Life</title>
    <link rel="stylesheet" href="../assets/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.rtl.min.css" integrity="sha384-gXt9imSW0VcJVHezoNQsP+TNrjYXoGcrqBZJpry9zJt8PCQjobwmhMGaDHTASo9N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
    
    <div class="row h-100">
        <div class="col-2 bg-dark dashboard-sidebar pe-0">
            <ul class="text-white ps-0">
                <li class="mt-3 p-3">
                    <a href="index.php">
                        <i class="fa fa-home align-middle" aria-hidden="true"></i>
                        <span>پیشخوان</span>
                    </a>
                </li>
                <li class="mt-3 p-3">
                    <a href="sliders.php">
                        <i class="fa fa-image align-middle" aria-hidden="true"></i>
                        <span>مدیریت اسلایدرها</span>
                    </a>
                </li>
                <li class="mt-3 p-3">
                    <a href="managePosts.php">
                        <i class="fa fa-newspaper align-middle" aria-hidden="true"></i>
                        <span>مدیریت نوشته ها</span>
                    </a>
                </li>
                <li class="mt-3 p-3">
                    <a href="usersManage.php">
                    <i class="fa fa-users align-middle" aria-hidden="true"></i>
                    <span>مدیریت کاربران</span>
                    </a>
                </li>
                <li class="mt-3 p-3">
                    <a href="categoryManage.php">
                        <i class="fa fa-list align-middle" aria-hidden="true"></i>
                        <span>دسته بندی ها</span>
                    </a>
                </li>
                <li class="mt-3 p-3">
                    <a href="../logout.php">
                        <i class="fas fa-sign-out-alt align-middle"></i>
                        <span>خروج</span>
                    </a>
                </li>
            </ul>
        </div>