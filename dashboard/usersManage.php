<?php
include_once "sidebar.php";

$stmt = $connect->prepare('SELECT * FROM users');
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<div class="col-10 dashboard-users pt-5">

    <div class="container">
        <i class="fas fa-pencil-alt fa-lg me-2"></i>
        <h2 class="d-inline-block">مدیریت کاربران</h2> 
        <hr>    
        <table class="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th class="text-center align-middle">#</th>
                    <th class="text-center align-middle">نام و نام خانوادگی</th>
                    <th class="text-center align-middle">ایمیل</th>
                    <th class="text-center align-middle">شماره موبایل</th>
                    <th class="text-center align-middle">دسترسی</th>
                    <th class="text-center align-middle">عملیات</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($users as $user){ ?>
                    <tr>
                        <td class="text-center align-middle" scope="row"><?php echo $user['id']; ?></td>
                        <td class="text-center align-middle"><?php echo $user['full_name']; ?></td>‍
                        <td class="text-center align-middle"><?php echo $user['email']; ?></td>‍
                        <td class="text-center align-middle"><?php echo $user['phone']; ?></td>‍
                        <td class="text-center align-middle">
                            <span>
                                <?php if($user['is_admin'] == '1'){
                                    echo 'ادمین';
                                }else{
                                    echo 'کاربر';
                                }?>
                            </span>
                        </td>‍
                        <td class="text-center align-middle">
                            <a href="#">
                                <i class="fas fa-trash fa-lg fa-fw align-middle text-danger"></i>
                            </a>                        
                        </td>‍
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    

</div>

<?php
 include_once "sidebar2.php"
?>