<?php
 include_once "sidebar.php";
?>
<?php
    $query = "SELECT posts.*,users.full_name FROM posts INNER JOIN users ON posts.owner_id=users.id ORDER BY created_at DESC";
    $stmt = $connect->prepare($query);
    $stmt->execute();
    $posts = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if($_SERVER['REQUEST_METHOD'] == "POST"){

        if($_POST['postId']){
            $postId = htmlspecialchars($_POST['postId']);
            $stmt2 = $connect->prepare('DELETE FROM posts WHERE id=:postId');
            $stmt2->bindParam(':postId',$postId);
            $stmt2->execute();
            $status = true;
            header("Location: $url/technolife/dashboard/managePosts.php?status=1");
        }
    }



?>
<div class="col-10 dashboard-posts pt-5">

            <div class="container mt-5">

                <?php if(isset($_GET['success'])){?>
                    <div class="alert alert-success" role="alert">
                        <span><?php echo $_GET['success']; ?></span>
                    </div>
                    
                <?php } ?>

                <?php if(isset($_GET['status'])){?>
                    <div class="alert alert-success" role="alert">
                        <span>نوشته مورد نظر با موفقیت حذف گردید</span>
                    </div>
                    
                <?php } ?>

                <i class="fas fa-pencil-alt fa-lg me-2"></i>
                <h2 class="d-inline-block">مدیریت نوشته ها</h2>
                <br><br>
                <a href="createPost.php" class="btn btn-outline-primary">
                    <span>ایجاد نوشته جدید</span>
                    <i class="fas fa-plus fa-fw align-middle"></i>
                </a>
                <hr class="my-5">
                <table class="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th class="text-center align-middle">#</th>
                            <th class="text-center align-middle">تصویر</th>
                            <th class="text-center align-middle">عنوان</th>
                            <th class="text-center align-middle">نویسنده</th>
                            <th class="text-center align-middle">تاریخ ایجاد</th>
                            <th class="text-center align-middle">آخرین بروزرسانی</th>
                            <th class="text-center align-middle">تعداد کامنت</th>                            
                            <th class="text-center align-middle">عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($posts as $key => $post){ ?>
                        <tr>
                            <td class="text-center align-middle" scope="row">
                                <?php echo $post['id']; ?>
                            </td>
                            <td class="text-center align-middle"><img class="img-fluid" src="https://via.placeholder.com/80" alt="no image"></td>
                            <td class="text-center align-middle">
                                <a href="#" class="text-white">
                                    <h5>
                                        <?php echo $post['title'] ?>
                                    </h5>
                                </a>
                            </td>
                            <td class="text-center align-middle">
                                <?php echo $post['full_name']; ?>
                            </td>
                            <td class="text-center align-middle">
                                <?php echo $post['created_at']; ?>
                            </td>
                            <td class="text-center align-middle">
                                <?php echo $post['updated_at']; ?>
                            </td>
                            <td class="text-center align-middle">
                                0
                            </td>
                            <td class="text-center align-middle">
                                <form action="managePosts.php" method="POST">
                                    <input type="hidden" value="<?php echo $post['id']; ?>" name="postId">
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fas fa-trash align-middle"></i>
                                    </button>
                                </form>
                                <br>
                                <a href="editPost.php?id=<?php echo $post['id']; ?>">
                                    <i class="fas fa-edit fa-lg fa-fw align-middle text-warning"></i>
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                
            </div>            
        </div>

<?php
 include_once "sidebar2.php"
?>