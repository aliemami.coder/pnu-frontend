<?php
 include_once "sidebar.php";
?>
<?php

$query = "SELECT * FROM posts ORDER BY created_at DESC";
$stmt = $connect->prepare($query);
$stmt->execute();

$posts = $stmt->fetchAll(PDO::FETCH_ASSOC);


?>

<div class="col-10 dashboard-index">

<div class="container mt-5">            
    <div class="row pt-5">

        <div class="col-6">
            <div class="boxes rounded border border-primary p-4 shadow">
                <div>
                    <i class="fas fa-pencil-alt"></i>
                    <h3 class="d-inline-block">آخرین نوشته ها</h3>
                </div>
                <hr>
                <ul>
                    <?php foreach($posts as $post){ ?>
                    <li class="row my-4">
                        <div class="col-auto">
                            <a href="#">
                                <img class="img-fluid d-inline-block" src="https://via.placeholder.com/80" alt="no image">
                            </a>
                        </div>
                        <div class="col-auto">
                            <a href="#">
                                <h4 class="d-inline-block"><?php echo $post['title']; ?></h4>
                            </a>
                            <br>
                            <i class="fas fa-user align-middle"></i>
                            <small>نویسنده: علی امامی</small>
                        </div>
                        <br>
                    </li> 
                    <?php } ?>                   
                </ul>
            </div>
        </div>
        <div class="col-6">
            <div class="boxes rounded border border-primary p-4 shadow">
                <div>
                    <i class="fas fa-pencil-alt"></i>
                    <h3 class="d-inline-block">آخرین دیدگاه ها</h3>
                </div>
                <hr>
                <ul>
                    <li>
                        <a href="#">
                            <h4 class="d-inline-block">عنوان دیدگاه </h4>
                        </a>
                        <br>
                        <i class="fas fa-user align-middle"></i>
                        <small>نویسنده: علی امامی</small>
                    </li>
                    <li class="my-5">
                        <a href="#">
                            <h4 class="d-inline-block">عنوان دیدگاه </h4>
                        </a>
                        <br>
                        <i class="fas fa-user align-middle"></i>
                        <small>نویسنده: علی امامی</small>
                    </li>
                    <li>
                        <a href="#">
                            <h4 class="d-inline-block">عنوان دیدگاه </h4>
                        </a>
                        <br>
                        <i class="fas fa-user align-middle"></i>
                        <small>نویسنده: علی امامی</small>
                        <br>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mt-5">
        
    </div>
</div>
</div>


<?php
 include_once "sidebar2.php";
?>