<?php
include_once "sidebar.php";



function categoryTree($parent_id = 0, $sub_mark = '')
{    
    global $connect;

    $query = "SELECT * FROM categories WHERE parent_id=:parent_id";
    $stmt = $connect->prepare($query);
    $stmt->bindParam(':parent_id',$parent_id);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);   
    foreach ($rows as $row) {
        echo '<option value="' . $row['id'] . '">' . $sub_mark . $row['name'] . '</option>';
        categoryTree($row['id'], $sub_mark . '---');        
    }    
}

$stmt3 = $connect->prepare("SELECT * FROM categories");
$stmt3->execute();
$categories = $stmt3->fetchAll(PDO::FETCH_ASSOC);


function getParentName($id)
{
    global $connect;
    if($id != 0){
        $stmt4 = $connect->prepare("SELECT name FROM categories WHERE id=:id");
        $stmt4->bindParam(':id',$id);
        $stmt4->execute();
        $row = $stmt4->fetch(PDO::FETCH_ASSOC);
        return $row['name'];
    }else{
        return 'دسته مادر';
    }
}


if($_SERVER['REQUEST_METHOD'] === "POST"){

    if(isset($_POST['categoryId'])){
        $id = htmlspecialchars($_POST['categoryId']);

        $removeCate = $connect->prepare('DELETE FROM categories WHERE id=:id');
        $removeCate->bindParam(':id',$id);
        $removeCate->execute();
        header("Location: $url/technolife/dashboard/categoryManage.php");

    }else{
        $name = htmlspecialchars($_POST['categoryName']);
        $parent_id = htmlspecialchars($_POST['parentCategory']);
    
        if($name != null && $parent_id != null){
            $q = "INSERT INTO categories (name,parent_id) VALUES (:name,:parent_id)";
            $stmt2 = $connect->prepare($q);
            $stmt2->bindParam(':name',$name);
            $stmt2->bindParam(':parent_id',$parent_id);
            $stmt2->execute();
            header("Location: $url/technolife/dashboard/categoryManage.php");
        }
    }

}

?>


<div class="col-10 dashboard-users pt-5">

    <div class="container">
        <i class="fas fa-pencil-alt fa-lg me-2"></i>
        <h2 class="d-inline-block">مدیریت دسته بندی ها</h2>

        <div class="my-4">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#categoryAdd">
                ایجاد دسته بندی جدید
            </button>


        </div>
        <!-- Modal -->
        <div class="modal fade" id="categoryAdd" tabindex="-1" role="dialog" aria-labelledby="categoryModalTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="categoryManage.php" method="POST">
                    <div class="modal-content bordered border-primary">
                        <div class="modal-header">
                            <h5 class="modal-title">افزودن دسته بندی جدید</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            
                                <div class="mb-3">
                                    <label for="categoryName" class="form-label">نام دسته بندی</label>
                                    <input type="text" class="form-control" name="categoryName" id="categoryName" placeholder="یک نام برای دسته بندی جدید وارد کنید">
                                </div>
                                <div class="mb-3">
                                    <label for="parentCate" class="form-label">دسته بندی مادر</label>
                                    <select class="form-select" name="parentCategory" id="parentCate">
                                        <option value="0" selected>دسته مادر</option>
                                        <?php categoryTree(); ?>
                                    </select>
                                </div>                            
                        
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">
                                <span>ذخیره</span>
                                <i class="fas fa-save align-middle"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <hr>
        <table class="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th class="text-center align-middle">#</th>
                    <th class="text-center align-middle">نام دسته بندی</th>
                    <th class="text-center align-middle">دسته بندی والد</th>
                    <th class="text-center align-middle">عملیات</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($categories as $key => $item){ ?>
                <tr>
                    <td class="text-center align-middle" scope="row"><?php echo ++$key; ?></td>
                    <td class="text-center align-middle"><?php echo $item['name']; ?></td>‍
                    <td class="text-center align-middle"><?php echo getParentName($item['parent_id']); ?></td>‍
                    <td class="text-center align-middle">
                        <form action="categoryManage.php" method="POST">
                            <input type="hidden" name="categoryId" value="<?php echo $item['id']; ?>">
                            <button type="submit" class="btn btn-danger">
                                <i class="fas fa-trash align-middle"></i>
                            </button>
                        </form>                        
                    </td>‍
                </tr>
                <?php }?>                
            </tbody>
        </table>
    </div>


</div>

<?php
include_once "sidebar2.php"
?>