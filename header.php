<?php 
session_start();
include_once "database.php";
include_once "generanDef.php";
?>

<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>تکنولایف</title>
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.rtl.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
</head>
<body>
<header>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark shadow">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <h2>تکنولایف</h2>
        </a>
        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav me-auto mt-2 mt-lg-0">
                <li class="nav-item mx-2">
                    <a class="nav-link text-white" href="index.php">صفحه اصلی </a>
                </li>                
                <li class="nav-item dropdown mx-2">
                    <a class="nav-link dropdown-toggle text-white" href="#" id="dropdownId" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">دسته بندی ها</a>
                    <div class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownId">
                        <a class="dropdown-item text-white" href="#">دسته بندی اول</a>
                        <a class="dropdown-item text-white" href="#">دسته بندی دوم</a>
                    </div>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link text-white" href="#">درباره ما</a>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link text-white" href="#">تماس با ما</a>
                </li>
            </ul>
            <div>
                <?php if(isset($_SESSION['username'])){ ?>
                    <div>
                        <a href="dashboard/index.php" class="btn btn-primary">
                            <span>پنل کاربری</span>
                            <i class="fas fa-user-cog fa-sm fa-fw align-middel"></i>
                        </a>
                    </div>  
                <?php }else{ ?>
                    <div class="row">
                        <div class="col-auto">
                            <a href="login.php" class="btn btn-info">
                                <span>ورود</span>
                                <i class="fas fa-sign-in-alt fa-sm fa-fw align-middle"></i>
                            </a>
                        </div>
                        <div class="col-auto">
                            <a href="register.php" class="btn btn-warning">
                                <span>ثبت نام</span>
                                <i class="fas fa-user-check fa-sm fa-fw align-middel"></i>
                            </a>
                        </div>                    
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</nav>
</header>   
